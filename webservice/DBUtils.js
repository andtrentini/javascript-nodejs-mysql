const { Sequelize, DataTypes, Model } = require('sequelize');
const { User } = require('./DBStructure.js');
var config = require('./config.js');


const sequelize = new Sequelize(config.database, config.user, config.password, {
    host: config.host,
    dialect: 'mysql',
    logging: (...msg) => console.log(msg)
  });

async function testConnection (response) {
    try {
        await sequelize.authenticate();
        console.log('Connessione stabilita con successo.');
        response.json('Connessione stabilita con successo.');
    } catch (error) {
        console.error('Impossibile connettersi al database:', error);
        response.json('Impossibile connettersi al database:', error);
    }
}

async function createDB(response) {        
    await User.sync({ force: true });
    console.log('Database creato.');
    response.json('Database creato.')
}

module.exports = {createDB, testConnection };