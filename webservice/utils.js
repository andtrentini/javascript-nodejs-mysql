function log(request, response, next) {
    let message = "Path richiesto: " + request.path + ', from: ' + request.ip;
    console.log(new Date().toLocaleString(), message);
    next(); 
}

module.exports = log;