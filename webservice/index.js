// Crea una istanza della classe Express che gestirà il nosro server
var service = require('./service.js');

// Parametri di configurazione del servizio
var config = require('./config.js');

// Metto in ascolto il server

var server = service.listen(config.serverPort, () => {
    console.log("WS Rest eample in esecuzione...");
})



