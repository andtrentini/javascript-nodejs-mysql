// Crea un collegamento con il modulo Express
var express = require('express');

// Crea una istanza della classe Cors (Cross origin Resource sharing)
var cors = require('cors');

// Crea una istanza della classe Compression
var compression = require('compression');

// Crea una istanza della classe Body-parser
var bodyParser = require('body-parser');

// Crea una istanna della classe JsonWebToken
var jwt = require('jsonwebtoken');

// Crea una istanza della classe BCrypt
var bcrypt = require('bcrypt-node');

// Carica nel servizio la funzione log
var log = require('./utils.js');

// Parametri di configurazione del servizio
var config = require('./config.js');

// Carica i router definiti negli altri file
var {createDB, testConnection} = require('./DBUtils.js');
const { response } = require('express');

var userRouter = require('./users.js');

// Crea una istanza della classe Express che gestirà il nosro server
var service = express();

// Carica la classe cors nel nostro servizio
service.use(cors());

// Carica la classe Compression nel nostro servizio
service.use(compression());

// Carica la classe BodyParser per analizzare il body della richiesta http
service.use(bodyParser.json());
service.use(bodyParser.urlencoded({ extended: false }));

var jsonParser = bodyParser.json()

// Carica la funzione log nel nostro servizio
service.use(log);

// Imposta l'elenco delle url alle quali rispondere con metodo GET
service.get('/', (request, response) => {
  response.sendFile(__dirname + '/help.html');
});

service.post('/login', (request, response) => {
  let datiLogin = request.body;
  if (datiLogin && datiLogin.username && datiLogin.password) {
    let username = datiLogin.username;
    let password = datiLogin.password;
    dbUtils.queryData(
      'SELECT * FROM Users Where username = ?',
      username,
      (errore, dati) => {
        if (dati && dati.length) {
          let encryptedPassword = dati[0].password;
          let name = dati[0].name;
          let surname = dati[0].surname;
          let role = dati[0].role;
          if (bcrypt.compareSync(password, encryptedPassword)) {
            var token = jwt.sign({
                name: name,
                surname: surname,
                username: username,
                role: role
              },
              config.secret, { expiresIn: 120 })
            response.status(200).send(token);
          } else {
            console.log(errore);
            response.status(401).send('Username o password errati');
          }
        } else {
          response.status(401).send('Username o password errati');
        }
      })
  } else {
    response.status(401).send(' Inviare username e password.')
  }
});

service.post('/createDB', (request, response) => {
  let dati = request.body;
  console.log(dati);
  if (dati && dati.key) {
    let key = dati.key;
    if (key == config.createKey) {
      createDB(response);
    }
    else {
      response.status(401).send('Per creare il database è necessario fornire la chiave corretta.');
    }
  }
  else {
    response.status(401).send('Per creare il database è necessario fornire la chiave corretta.');
  }
});

service.get('/testConnection', (request, response) => {
  
  testConnection(response);
})

service.use('/users', userRouter);

module.exports = service;