const { Sequelize, DataTypes, Model } = require('sequelize');
var config = require('./config.js');

const sequelize = new Sequelize(config.database, config.user, config.password, {
    host: config.host,
    dialect: 'mysql',
    logging: (...msg) => console.log(msg)
  });

class User extends Model {}
    
User.init({ 
        
    // Model attributes are defined here
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    surname: {
        type: DataTypes.STRING
        // allowNull defaults to true
    }
    }, {
        // Other model options go here
        sequelize, // We need to pass the connection instance
        modelName: 'User' // We need to choose the model name
});

module.exports = { User }