const { User } = require('./DBStructure.js');

//var auth = require('./auth.js');

var express = require('express');
var router = express.Router();

async function addUser(request, response) {
    let dati = request.body;  
    if (dati && dati.name && dati.surname) {
        let name = dati.name;
        let surname = dati.surname;
    
        const user = await User.create({ name: name, surname: surname });
        response.json(user);
    }
    else {
        response.status(400).send('Dati non corretti.');
    }  
}

async function getAllUsers(response) {
    const users = await User.findAll();    
    response.json(users);
}

async function getUser(request, response) {
    let id = request.params.id;
    const user = await User.findOne({ where: { id: id } });    
    response.json(user);
}

async function updateUser(request, response) {
    let id = request.params.id;
    let dati = request.body;  
    if (dati && dati.name && dati.surname) {
        let name = dati.name;
        let surname = dati.surname;
        const result = await User.update({ name: name, surname: surname }, {
            where: {
              id: id
            }
          });        
        if (result[0] == 1) {
            const user = await User.findAll({
                where: {
                    id: id
                  }
            });    
            response.json(user);
        }
        else {
            response.status(400).send('Utente non trovato.');
        }
    }
    else {
        response.status(400).send('Dati non corretti.');
    }
}

async function deleteUser(request, response) {
    let id = request.params.id;
    const result = await User.destroy({
        where: {
          id: id
        }
      });
    if (result > 0) {
        response.json('Utente eiminato.')
    }
    else {
        response.status(400).send('Utente non trovato.');
    }    
}

router.get('/', (request, response) => {
    getAllUsers(response);
})

router.get('/:id', (request, response) => {
    getUser(request, response);
})

router.post('/', (request, response) => {
    addUser(request, response);
});

router.put('/:id', (request, response) => {
    updateUser(request, response);
})

router.delete('/:id', (request, response) => {
    deleteUser(request, response);
})
//router.use(auth);

module.exports = router;