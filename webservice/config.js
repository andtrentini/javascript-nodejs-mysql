var config = {
  host: 'mysql-example',
  user: 'example',
  password: 'root',
  database: 'example',
  serverPort: 3000,
  createKey: 123456789,
  secret: 'Questa è la stringa con cui codificare il token'
}

module.exports = config;